const axios = require("axios");

class BurgerBotAPI {
  // Save the order details to firestore
  static saveClientOrder(params) {
    return axios.post(
      `${process.env.BURGERBOT_API_ENDPOINT}/save-order`,
      params
    );
  }
}

module.exports = { BurgerBotAPI };
