class BurgerBotAPIMock {
  static getBurgerTypes() {
    //
    // 1	Big Mac
    // 2	Mac Chicken
    // 3	Mac Fish
    // 4	Deluxe
    // 5	Mac First
    // 6	Burger
    // 7	Cheese Burger

    const burgerTypes = [
      { id: 1, label: "Big Mac" },
      { id: 2, label: "Mac Chicken" },
      { id: 3, label: "Mac Fish" },
      { id: 4, label: "Deluxe" },
      { id: 5, label: "Mac First" },
      { id: 6, label: "Burger" },
      { id: 6, label: "Cheese Burger" }
    ];

    return burgerTypes;
  }

  static getMenuBurgerTypes() {
    // Big Mac
    // Mac Chicken
    // Mac Fish
    // Deluxe
    // Mac First
    // Happy Meal

    const menuBurgerTypes = [
      { id: 1, label: "Big Mac" },
      { id: 2, label: "Mac Chicken" },
      { id: 3, label: "Mac Fish" },
      { id: 4, label: "Deluxe" },
      { id: 5, label: "Mac First" },
      { id: 6, label: "Happy Meal" }
    ];

    return menuBurgerTypes;
  }

  static getBurgerSize() {
    const burgerSizes = [
      { id: 1, label: "Best of" },
      { id: 2, label: "Maxi Best of" }
    ];

    return burgerSizes;
  }
}

module.exports = { BurgerBotAPIMock };
