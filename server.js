const { WebhookClient } = require("dialogflow-fulfillment");
const orderHandler = require("./handlers/OrderHandler");
const orderSelectAnOrderTypeHandler = require("./handlers/OrderSelectAnOrderTypeHandler");
const askForAMenuTypeHandler = require("./handlers/AskForAMenuTypeHandler");
const askForABurgerTypeHandler = require("./handlers/AskForABurgerTypeHandler");
const askForAMenuTypeSelectAMenuTypeHandler = require("./handlers/AskForAMenuTypeSelectAMenuTypeHandler");
const askForABurgerTypeSelectABurgerTypeHandler = require("./handlers/AskForABurgerTypeSelectABurgerTypeHandler");
const askForBurgerSizeHandler = require("./handlers/AskForBurgerSizeHandler");
const askForBurgerSizeSelectBurgerSizeHandler = require("./handlers/AskForBurgerSizeSelectBurgerSizeHandler");
const askForDrinkHandler = require("./handlers/AskForDrinkHandler");
const askForDeliveryLocationHandler = require("./handlers/AskForDeliveryLocationHandler");
const askForDeliveryLocationCheckLocationHandler = require("./handlers/AskForDeliveryLocationCheckLocationHandler");
const askForClientNameHandler = require("./handlers/AskForClientNameHandler");
const orderConfirmationHandler = require("./handlers/OrderConfirmationHandler");

require("dotenv").config();

const express = require("express");
const app = express();

let INTENT_REGISTRY = new Map();

app.get("/", (request, response) => response.send("online"));
app.post("/dialogflow", express.json(), (request, response) => {
  const agent = new WebhookClient({ request, response });

  agent.handleRequest(INTENT_REGISTRY);
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  // Add function handler to the map for each Intent
  INTENT_REGISTRY.set("Order", orderHandler);

  INTENT_REGISTRY.set(
    "Order - select an Order Type",
    orderSelectAnOrderTypeHandler
  );

  INTENT_REGISTRY.set("Ask For A Menu Type", askForAMenuTypeHandler);

  INTENT_REGISTRY.set(
    "Ask For A Menu Type - Select A Menu Type",
    askForAMenuTypeSelectAMenuTypeHandler
  );

  INTENT_REGISTRY.set("Ask For A Burger Type", askForABurgerTypeHandler);

  INTENT_REGISTRY.set(
    "Ask For A Burger Type - Select A Burger Type",
    askForABurgerTypeSelectABurgerTypeHandler
  );

  INTENT_REGISTRY.set("Ask For Drink", askForDrinkHandler);

  INTENT_REGISTRY.set("Ask For Burger Size", askForBurgerSizeHandler);
  INTENT_REGISTRY.set(
    "Ask For Burger Size - Select Burger Size",
    askForBurgerSizeSelectBurgerSizeHandler
  );

  INTENT_REGISTRY.set(
    "Ask For Delivery Location",
    askForDeliveryLocationHandler
  );
  INTENT_REGISTRY.set(
    "Ask For Delivery Location - Check Location",
    askForDeliveryLocationCheckLocationHandler
  );

  INTENT_REGISTRY.set("Ask For Client Name", askForClientNameHandler);

  INTENT_REGISTRY.set("Order Confirmation", orderConfirmationHandler);

  console.log(`App listening on port ${PORT}`);
  console.log("Press Ctrl+C to quit.");
});
