# Instructions

## Overview

- [Development environment setup](#development-environment-setup)
  1. [Npm installation](#1.-npm-installation)
  2. [Ngrok installation](#2.-ngrok-installation)
  3. [DOJO template](#4.-dojo-template)
  4. [DialogFlow account creation](#3.-dialogflow-account-creation)
  5. [Import DialogFlow Agent](#5.-import-dialogFlow-agent-and-configure-webhook-fulfillment)
- [TODO 1](#todo-1)
- [TODO 2](#todo-2)

## Development environment setup

### 1. Npm installation

```
# npm is installed with node

https://www.npmjs.com/get-npm
```

### 2. Ngrok installation

https://ngrok.com/download

```
# for mac
brew install ngrok

# for windows
npm install ngrok
```

### 3. DOJO template

```
>git clone https://gitlab.com/uprate-meetups/dialogflow
```

Install the project dependencies

```
>npm install
```

Start the webhook projet

```
>cd burger-bot
>npm run start:dev
```

### 3. DialogFlow account creation

- Create a new account on https://dialogflow.cloud.google.com
- Create a new Agent `BurgerBot`

### 5. Import DialogFlow Agent and configure Webhook fulfillment

Import the `BurgerBot.zip` file to Dialogflow

`Agent Settings > Export and Import > RESTORE FROM ZIP`

Start an Ngrok proxy

```
>ngrok http 4000
```

Copy the https link from Ngrok

Go the `Fulfillment -> URL*` and replace https://xxxxxxx.ngrok.io with the copied ngrok link
**`(KEEP /dialogflow AT THE END OF THE URL)`**

Test your agent at https://dialogflow.cloud.google.com/#/assistant_preview

---

## TODO 1

---

**The first intent (Welcome Intent) is handled by `OrderHandler.js`**

1. Open `OrderHandler.js` file
2. Get the conversation object `conv` from the DialogFlow agent

```javascript
let conv = agent.conv();
```

3. Welcome the user and ask him which type of order did he prefer

```javascript
conv.ask("......"):
```

4. Give the user two choices : Menu, Burger

Adapt the following code snippet to meet the requirement.

```javascript
conv.ask(
  new List({
    items: {
      // Add the first item to the list
      SELECTION_KEY_ONE: {
        title: "Title of First List Item",
        synonyms: ["synonym 1", "synonym 2", "synonym 3"]
      },
      // Add the second item to the list
      SELECTION_KEY_TWO: {
        title: "Title of Second List Item",
        synonyms: ["Google Home Assistant", "Assistant on the Google Home"]
      }
    }
  })
);
```

5. Serialize the conversation and hand it back to the agent.

```javascript
const payloadData = conv.serialize();
agent.add(new Payload("ACTIONS_ON_GOOGLE", payloadData.payload.google));
```

**The second intent (Get the user choice) is handled by `OrderSelectAnOrderTypeHandler.js`**

6. Open OrderSelectAnOrderTypeHandler.js file

7. agent.getContext("actions_intent_option").parameters.OPTION contains the user choice, store it in variable.

```javascript
const orderType = agent.getContext("actions_intent_option").parameters.OPTION;
```

8. Follow up to the right intent (by triggering the right event) according to the user choice.

```javascript
agent.setFollowupEvent("EVENT_NAME");
```

`AskForAMenuTypeEvent` is the event that trigger `Ask For A Menu Type` Intent

`AskForABurgerTypeEvent` is the event that trigger `Ask For A Burger Type` Intent

---

## TODO 2

---

1. First, configure the `DrinkType` and `DrinkCategory` entities on DialogFlow

| DrinkType    |                                  |
| ------------ | -------------------------------- |
| coca-cola    | coca, coca-cola                  |
| eau plate    | eau plate                        |
| eau gazeuse  | eau gazeuse                      |
| fanta        | fanta                            |
| sprite       | sprite                           |
| jus d'orange | jus d'orange, minute maid orange |

<br/>
<br/>

| DrinkCategory |       |
| ------------- | ----- |
| eau           | eau   |
| jus           | jus   |
| sodas         | sodas |

2. Create `Ask For Drink` intent and add `AskForDrinkEvent`
3. Add as many training phrases as you can, for example :

   - Je veux un jus
   - Je prend un jus d'orange
   - coca s'il vous plaît
   - ...

- Annotate the training phrases with the created entities `DrinkType` and `DrinkCategory`
- In the **Action and parameters** section, choose `drinkType`, `drinkCategory` as parameters names

4.  Enable Webhook call for "Ask For Drink"
5.  Enable Webhook call for slot filling

6.  Implement the handler `AskForDrinkHandler.js`

- Get "drinkType, drinkCategory" parameters from the agent

```javascript
agent.parameters("PARAMETER_NAME");
```

7. If `drinkType` is not empty, store it to the global context `burgerbot-context` and follow up to the event `AskForBurgerSizeEvent`, otherwise keep asking the user for the drink

- For example, if the user provide only the drinkCategory, ask him back another question like :

```javascript
agent.add(`Quel type de ${drinkCategory} souhaitez-vous ?`);
```

- To store a variable to the global context, you can user the following statement :

```javascript
agent.setContext({
  name: "burgerbot-context",
  lifespan: 5,
  parameters: { DRINK: drinkType }
});
```

# Congratulations ! You can order you burger now 🍔, bon appétit !
