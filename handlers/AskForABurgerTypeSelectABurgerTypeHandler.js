/**
 * Handler for 'Ask For A Burger Type Select A Burger Type Handler'
 * @param agent WebhookClient
 */
function askForABurgerTypeSelectABurgerTypeHandler(agent) {
  console.log("INTENT HANDLER >> AskForABurgerTypeSelectABurgerTypeHandler");

  if (agent.getContext("actions_intent_option").parameters.OPTION) {
    let selectedBurgerType = agent.getContext("actions_intent_option")
      .parameters.text;

    console.log(`Selected burger type ${selectedBurgerType}`);

    agent.setContext({
      name: "burgerbot-context",
      lifespan: 5,
      parameters: { BURGER_TYPE: selectedBurgerType }
    });

    console.log("FOLLOW UP >> AskForDeliveryLocationEvent");
    agent.setFollowupEvent("AskForDeliveryLocationEvent");
  }
}

module.exports = askForABurgerTypeSelectABurgerTypeHandler;
