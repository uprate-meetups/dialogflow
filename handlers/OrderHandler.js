const { Payload } = require("dialogflow-fulfillment");
const { List } = require("actions-on-google");

/**
 * Handler for 'Order Handler'
 * @param agent WebhookClient
 */
function orderHandler(agent) {
  console.log("INTENT HANDLER >> OrderHandler");

  let conv = agent.conv();

  conv.ask("Bienvenue chez Burger Bot, que souhaitez-vous commander ?\n");

  conv.ask(
    new List({
      items: {
        menu: { title: "Un Menu", synonyms: ["Menu", "un Menu"] },
        burger: {
          title: "Burger seul",
          synonyms: ["Un burger seul", "Burger"]
        }
      }
    })
  );

  const payloadData = conv.serialize();
  agent.add(new Payload("ACTIONS_ON_GOOGLE", payloadData.payload.google));
}

module.exports = orderHandler;
