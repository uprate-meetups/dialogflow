const { Payload } = require("dialogflow-fulfillment");
const { List, SimpleResponse } = require("actions-on-google");
const { BurgerBotAPIMock } = require("../api/BurgerBotAPIMock");

/**
 * Handler for 'Ask For Burger Size Handler'
 * @param agent WebhookClient
 */
async function askForBurgerSizeHandler(agent) {
  console.log("INTENT HANDLER >> AskForBurgerSizeHandler");
  let conv = agent.conv();

  let choices = {};

  const burgerSizes = BurgerBotAPIMock.getBurgerSize();

  burgerSizes.forEach(burgerSize => {
    choices[burgerSize.id] = {
      title: burgerSize.label,
      synonyms: [burgerSize.label]
    };
  });

  conv.ask(
    new SimpleResponse({
      speech: `En quelle taille ?\n`,
      text: "En quelle taille ?\n"
    })
  );

  conv.ask(
    new List({
      items: choices
    })
  );

  const payloadData = conv.serialize();
  agent.add(new Payload("ACTIONS_ON_GOOGLE", payloadData.payload.google));
}

module.exports = askForBurgerSizeHandler;
