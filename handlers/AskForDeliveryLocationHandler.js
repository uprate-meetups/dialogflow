const { Payload, Suggestion } = require("dialogflow-fulfillment");
const { Permission } = require("actions-on-google");

/**
 * Handler for 'Ask For Delivery Location Handler'
 * @param agent WebhookClient
 */
function askForDeliveryLocationHandler(agent) {
  console.log("INTENT HANDLER >> AskForDeliveryLocationHandler");

  let conv = agent.conv();

  conv.ask(
    new Permission({
      context: "Pour vous livrer ",
      permissions: "DEVICE_PRECISE_LOCATION"
    })
  );

  const data = conv.serialize();

  agent.add(new Payload("ACTIONS_ON_GOOGLE", data.payload.google));
  agent.add(new Suggestion("Oui"));
  agent.add(new Suggestion("Non"));
}

module.exports = askForDeliveryLocationHandler;
