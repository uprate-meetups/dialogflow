const { Payload } = require("dialogflow-fulfillment");
const { BurgerBotAPI } = require("../api/BurgerBotAPI");

/**
 * Handler for 'Order Confirmation Handler'
 * @param agent WebhookClient
 */
async function orderConfirmationHandler(agent) {
  console.log("INTENT HANDLER >> OrderConfirmationHandler");

  // Get the order information from the global context : burgerbot-context

  const {
    CLIENT_NAME,
    CLIENT_ADDRESS,
    BURGER_TYPE,
    MENU_BURGER_TYPE,
    DRINK,
    BURGER_SIZE
  } = agent.getContext("burgerbot-context").parameters;

  let clientOrder = "";

  if (BURGER_TYPE) {
    clientOrder = `${BURGER_TYPE}`;
  } else {
    clientOrder = `${MENU_BURGER_TYPE} en ${BURGER_SIZE} avec ${DRINK}`;
  }

  // Save the client order to firestore
  let orderParams = {
    clientName: CLIENT_NAME,
    burgerType: BURGER_TYPE,
    menuBurgerType: MENU_BURGER_TYPE,
    burgerSize: BURGER_SIZE,
    drink: DRINK,
    clientAddress: CLIENT_ADDRESS
  };

  try {
    let response = await BurgerBotAPI.saveClientOrder(orderParams);

    if (response.status == 200) {
      let conv = agent.conv();
      conv.close(
        `Merci ${CLIENT_NAME}, votre commande ${clientOrder} est en cours de préparation. Vous serez livré à l’adresse ${CLIENT_ADDRESS} d’ici environ 10 minutes. A très vite !`
      );

      const data = conv.serialize();
      agent.add(new Payload("ACTIONS_ON_GOOGLE", data.payload.google));
    }
  } catch (error) {
    console.log(error);
  }
}

module.exports = orderConfirmationHandler;
