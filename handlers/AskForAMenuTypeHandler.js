const { Payload } = require("dialogflow-fulfillment");
const { List, SimpleResponse } = require("actions-on-google");
const { BurgerBotAPIMock } = require("../api/BurgerBotAPIMock");

/**
 * Handler for 'Ask For A Menu Type Handler'
 * @param agent WebhookClient
 */
async function askForAMenuTypeHandler(agent) {
  console.log("INTENT HANDLER >> AskForAMenuTypeHandler");
  let conv = agent.conv();

  let choices = {};

  const menuBurgerTypes = BurgerBotAPIMock.getMenuBurgerTypes();

  menuBurgerTypes.forEach(menuBurgerType => {
    choices[menuBurgerType.id] = {
      // key
      title: menuBurgerType.label,
      synonyms: [menuBurgerType.label]
    };
  });

  conv.ask(
    new SimpleResponse({
      speech: `Quel menu souhaitez-vous ?\n`,
      text: `Quel menu souhaitez-vous ?\n`
    })
  );

  conv.ask(
    new List({
      items: choices
    })
  );

  const payloadData = conv.serialize();
  agent.add(new Payload("ACTIONS_ON_GOOGLE", payloadData.payload.google));
}

module.exports = askForAMenuTypeHandler;
