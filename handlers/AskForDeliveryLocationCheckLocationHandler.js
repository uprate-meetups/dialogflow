const { Payload } = require("dialogflow-fulfillment");

/**
 * Handler for 'Ask For Delivery Location Check Location Handler'
 * @param agent WebhookClient
 */
function askForDeliveryLocationCheckLocationHandler(agent) {
  console.log("INTENT HANDLER >> AskForDeliveryLocationCheckLocationHandler");

  let conv = agent.conv();

  let device = conv.request.device;
  let permissions = conv.user.permissions;

  if (
    typeof permissions !== undefined &&
    permissions.indexOf("DEVICE_PRECISE_LOCATION") >= 0
  ) {
    // If we requested precise location, it means that we're on a phone.
    // Because we will get only latitude and longitude, we need to
    // reverse geocode to get the city.
    let { formattedAddress } = device.location;

    agent.setContext({
      name: "burgerbot-context",
      lifespan: 5,
      parameters: {
        CLIENT_ADDRESS: formattedAddress.substring(
          0,
          formattedAddress.indexOf(",")
        )
      }
    });

    // Followup to askForClientName intent
    console.log("FOLLOW UP >> AskForClientNameEvent");
    agent.setFollowupEvent("AskForClientNameEvent");
  } else {
    let conv = agent.conv();
    conv.close("Dommage, nous espérons vous revoir une prochaine fois.");

    const data = conv.serialize();
    agent.add(new Payload("ACTIONS_ON_GOOGLE", data.payload.google));
  }
}

module.exports = askForDeliveryLocationCheckLocationHandler;
