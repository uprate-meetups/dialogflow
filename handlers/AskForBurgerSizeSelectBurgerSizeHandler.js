/**
 * Handler for 'Ask For Burger Size Select Burger Size Handler'
 * @param agent WebhookClient
 */
function askForBurgerSizeSelectBurgerSizeHandler(agent) {
  console.log("INTENT HANDLER >> AskForBurgerSizeSelectBurgerSizeHandler");
  if (agent.getContext("actions_intent_option").parameters.OPTION) {
    let selectedBurgerType = agent.getContext("actions_intent_option")
      .parameters.text;

    agent.setContext({
      name: "burgerbot-context",
      lifespan: 5,
      parameters: { BURGER_SIZE: selectedBurgerType }
    });

    console.log("FOLLOW UP >> AskForDeliveryLocationEvent");
    agent.setFollowupEvent("AskForDeliveryLocationEvent");
  }
}

module.exports = askForBurgerSizeSelectBurgerSizeHandler;
