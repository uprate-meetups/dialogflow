const { Payload } = require("dialogflow-fulfillment");

/**
 * Handler for 'Ask For Drink Handler'
 * @param agent WebhookClient
 */
async function askForDrinkHandler(agent) {
  console.log("INTENT HANDLER >> AskForDrinkHandler");
  const conv = agent.conv();

  const { drinkType, drinkCategory } = agent.parameters;

  console.log(
    `Slot filling parameters : drinkType => [${drinkType}], drinkCategory => [${drinkCategory}]`
  );

  if (drinkType) {
    console.log(`Selected drink type : ${drinkType}`);

    // Set the selected drink to the global context
    agent.setContext({
      name: "burgerbot-context",
      lifespan: 5,
      parameters: { DRINK: drinkType }
    });

    console.log("FOLLOW UP >> AskForBurgerSizeEvent");
    agent.setFollowupEvent("AskForBurgerSizeEvent");
  } else if (!drinkType && drinkCategory) {
    console.log(`Selected drink category : ${drinkType}`);

    conv.ask(`Quel type de ${drinkCategory} souhaitez-vous ?`);
  } else {
    console.log(`Ask for the desired drink`);
    conv.ask("La boisson ?");
  }

  conv.ask(" ");
  const payloadData = conv.serialize();
  agent.add(new Payload("ACTIONS_ON_GOOGLE", payloadData.payload.google));
}

module.exports = askForDrinkHandler;
