const { Payload } = require("dialogflow-fulfillment");
const { List, SimpleResponse } = require("actions-on-google");
const { BurgerBotAPIMock } = require("../api/BurgerBotAPIMock");

/**
 * Handler for 'Ask For A Burger Type Handler'
 * @param agent WebhookClient
 */
async function AskForABurgerTypeHandler(agent) {
  console.log("INTENT HANDLER >> AskForABurgerTypeHandler");

  let conv = agent.conv();

  let choices = {};

  const burgerTypes = BurgerBotAPIMock.getBurgerTypes();

  // Build a choice list from BurgerBotAPIMock
  burgerTypes.forEach(burgerType => {
    // key
    choices[burgerType.id] = {
      title: burgerType.label,
      synonyms: [burgerType.label]
    };
  });

  conv.ask(
    new SimpleResponse({
      speech: `Quel burger souhaitez-vous ?\n`,
      text: "Quel burger souhaitez-vous ?\n"
    })
  );

  conv.ask(
    new List({
      items: choices
    })
  );

  const payloadData = conv.serialize();
  agent.add(new Payload("ACTIONS_ON_GOOGLE", payloadData.payload.google));
}

module.exports = AskForABurgerTypeHandler;
