/**
 * Handler for 'Ask For A Menu Type Select A Menu Type Handle'
 * @param agent WebhookClient
 */
function askForAMenuTypeSelectAMenuTypeHandler(agent) {
  console.log("INTENT HANDLER >> AskForAMenuTypeSelectAMenuTypeHandler");

  if (agent.getContext("actions_intent_option").parameters.OPTION) {
    let selectedMenuBurgerType = agent.getContext("actions_intent_option")
      .parameters.text;

    console.log(`Menu selected : ${selectedMenuBurgerType}`);
    agent.setContext({
      name: "burgerbot-context",
      lifespan: 5,
      parameters: { MENU_BURGER_TYPE: selectedMenuBurgerType }
    });

    // Followup to Ask for Drink choice
    console.log("FOLLOW UP >> AskForDrinkEvent");
    agent.setFollowupEvent("AskForDrinkEvent");
  }
}

module.exports = askForAMenuTypeSelectAMenuTypeHandler;
