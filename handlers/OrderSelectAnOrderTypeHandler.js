/**
 * Handler for 'Order Select An Order Type Handler'
 * @param agent WebhookClient
 */
function OrderSelectAnOrderTypeHandler(agent) {
  console.log("INTENT HANDLER >> OrderSelectAnOrderTypeHandler");
  if (agent.getContext("actions_intent_option").parameters.OPTION) {
    let selectedOrderType = agent.getContext("actions_intent_option").parameters
      .OPTION;

    switch (selectedOrderType) {
      case "menu":
        console.log("FOLLOW UP >> AskForAMenuTypeEvent");
        agent.setFollowupEvent("AskForAMenuTypeEvent");
        break;
      case "burger":
        console.log("FOLLOW UP >> AskForABurgerTypeEvent");
        agent.setFollowupEvent("AskForABurgerTypeEvent");
        break;
    }
  }
}

module.exports = OrderSelectAnOrderTypeHandler;
