/**
 * Handler for 'Ask For Client Name Handler'
 * @param agent WebhookClient
 */
function askForClientNameHandler(agent) {
  console.log("INTENT HANDLER >> AskForClientNameHandler");
  let { clientName } = agent.parameters;

  if (clientName) {
    agent.setContext({
      name: "burgerbot-context",
      lifespan: 5,
      parameters: { CLIENT_NAME: clientName }
    });

    console.log("FOLLOW UP >> OrderConfirmationEvent");
    agent.setFollowupEvent("OrderConfirmationEvent");
  } else {
    agent.add("Parfait, nous y sommes presque. \n" + "Un prénom ?\n");
  }
}

module.exports = askForClientNameHandler;
